<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @wordpress-plugin
 * Plugin Name:       Ajaxify Comments
 * Plugin URI:        https://bitbucket.org/thatnguy/ajaxify-comments
 * Description:       Creates an AJAX Comment system for WordPress.
 * Version:           1.0.0
 * Author:            phong
 * Author URI:        https://bitbucket.org/thatnguy
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ajaxify-comments
 * Domain Path:       /languages
 */

/**
 * NOTE: a pretty permalink should be used for WP-API to function. do not use the first permalink settings in admin i.e ?p=
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


function activate_ajaxify_comments() {
	// WP-API needs to be activated.
	if ( ! is_plugin_active( 'rest-api/plugin.php' ) ) {
		deactivate_plugins( plugin_basename( __FILE__ ) );

		wp_die(
			sprintf( 
				__( 'This plugin requires WP-API plugin. Check it out %s', 'ajaxify-comments' ),
				'<a href="https://wordpress.org/plugins/json-rest-api/">here</a>'
			),
			'Requirements',
			array( 'back_link' => true )
		);
	}

}
register_activation_hook( __FILE__, 'activate_ajaxify_comments' );

// load our plugin core.
require_once plugin_dir_path( __FILE__ ) . 'inc/ajaxify-comments.class.php';

// initialize first.
new Ajaxify_Comments();