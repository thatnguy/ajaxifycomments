<?php
/**
 * Plugin Core
 */

class Ajaxify_Comments {

	private $version = '1.0.0';

	private $api_namespace = 'ajaxify-comments/api';

	private $post_id = 0;

	private $args = array(
		'per_page'             => 5,
		'orderby'              => '',
		'order'                => 'desc',
		'children_fetch_count' => 3,
	);

	private $sortables = array(
		'date',
		'date_gmt',
		'id',
		'include',
		'post',
		'parent',
		'type',
	);

	// whether the sortables 'asc' will prepend the comment or append ( default )
	private $asc_prepend = array();

	private $wp_api_url = '';

	private $api_url = '';

	private $dir_path = '';

	private $dir_uri = '';

	private $template_path = '';

	public static $instance = null;


	public function __construct() {
		
		// setup plugin path.
		$this->dir_path = apply_filters( 'ajaxify_comments_path', plugin_dir_path( dirname( __FILE__ ) ) );
		$this->dir_uri = apply_filters( 'ajaxify_comments_uri', plugin_dir_url( dirname( __FILE__ ) ) );

		$this->template_path = "{$this->dir_path}tmpl";

		// setup api url
		// one is used for WP API and one for our own altered comments route.
		$site_url = site_url();
		$this->wp_api_url = "{$site_url}/wp-json/wp/v2";
		$this->api_url = "{$site_url}/wp-json/{$this->api_namespace}";

		// setup hooks
		if ( ! self::$instance ) 
			add_action( 'init', array( $this, 'setup_hooks' ) );
	
	}

	public function set_post( $post_id ) {
		$this->post_id = $post_id;
	}

	public function set_arg( $name, $value ) {
		if ( isset( $this->args[ $name ] ) )
			$this->args[ $name ] = $value;
	}

	// setup hooks
	public function setup_hooks() {

		add_filter( 'rest_prepare_comment', array( $this, 'rest_prepare_comments' ), 10, 3 );

		// rendering scripts.
		add_action( 'wp_enqueue_scripts', array( $this, 'header_scripts' ) );

		// template tags.
		add_action( 'wp_head', array( $this, 'template_tags' ) );

		add_action( 'rest_api_init', array( $this, 'register_routes' ) );

	}

	public function register_routes( $server ) {
		require_once $this->dir_path . '/inc/comments-controller.class.php';

		$controller = new Ajaxify_Comments_Controller( $this->api_namespace );

		$controller->register_routes();
	}

	public function rest_prepare_comments( $response, $comment, $request ) {
		$data = $response->get_data();

		// can user moderate comments
		$data['can_moderate'] = false;

		// returns 0 if no current user found.
		$user_id = get_current_user_id();

		// check if user can moderate the comments.
		if ( $user_id ) {
			if ( current_user_can( 'moderate_comments' ) || $user_id === $data['author'] )
				$data['can_moderate'] = true;
		}

		// make the date look presentable
		$data['human_date'] = format_comment_date( $data['date'] );

		// allow devs to filter the prepared comments data.
		$data = apply_filters( 'ajaxify_comments_rest_prepare_comments', $data, $comment );

		$response->set_data( $data );

		return $response;
	}

	private function enqueue_scripts( $scripts = array(), $footer = true ) {
		if ( ! is_array( $scripts ) )
			return false;

		foreach ( $scripts as $handler => $settings ) {
			if ( ! is_array( $settings ) )
				continue;

			if ( ! isset( $settings['url'] ) || empty( $settings['url'] ) )
				continue;

			$_settings = wp_parse_args( $settings, array( 'dep' => array(), 'type' => 'js' ) );

			if ( 'js' === $_settings['type'] )
				wp_enqueue_script( $handler, $settings['url'], $_settings['dep'], '', $footer );
			else
				wp_enqueue_style( $handler, $settings['url'], $_settings['dep'] );
		}
	}

	public function header_scripts() {
		// main style script.
		wp_enqueue_style( 'ajaxify-comments', "{$this->dir_uri}css/ajaxify-comments.css" );

		$scripts = apply_filters( 'ajaxify_comments_header_script', array() );

		$this->enqueue_scripts( $scripts, false );
	}

	public function prepare_scripts() {
		add_action( 'wp_footer', array( $this, 'footer_scripts' ) );
	}

	public function footer_scripts() {
		// print templates.
		$this->print_templates();

		// polyfills.
		wp_enqueue_script( 'ajaxify-comments-polyfills', "{$this->dir_uri}js/polyfills.js", array(), '', true );
		
		// main script.
		wp_enqueue_script( 'ajaxify-comments', "{$this->dir_uri}js/ajaxify-comments.js", array( 'backbone' ) );

		// localize script data.
		$data = array(
			'post' => $this->post_id,
			'api_urls'  => array( 'wp' => $this->wp_api_url, 'ajaxify-comments' => $this->api_url )
		);

		foreach ( $this->args as $key => $value ) {
			$data[ $key ] = $value;
		}

		// WP Rest nonce needed for cookie authentication.
		$data['rest_nonce'] = wp_create_nonce( 'wp_rest' );

		// is user logged in.
		$data['user_logged_in'] = is_user_logged_in();

		// only these sortables can be used for ordering.
		$sortables = apply_filters( 'ajaxify_comments_sortables', $this->sortables );
		$asc_prepend = apply_filters( 'ajaxify_comments_asc_prepend', $this->asc_prepend );

		$data['sortables'] = array();
		foreach ( $sortables as $key => $value ) {
			$data['sortables'][ $value ] = isset( $asc_prepend[ $value ] ) ? false : true;
		}

		if ( empty( $data['orderby'] ) && ! empty( $this->sortables ) ) {
			$data['orderby'] = $this->sortables[0];
		}

		wp_localize_script( 'ajaxify-comments', 'ajaxify_comments_data', $data );

		$scripts = apply_filters( 'ajaxify_comments_enqueue_scripts', array() );

		$this->enqueue_scripts( $scripts );

	}

	public function template_tags() {
		require_once "{$this->dir_path}inc/template-tags.php";
	}

	// start here
	public function render() {

		$this->prepare_scripts();

		// because render() can be called anywhere inside the content we should
		// setup a mark point in the DOM tree so we can replace with a container.
		?><div id="ajaxify-comments-startpoint"></div><?php
	}

	public static function get_instance() {
		
		if ( is_null( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function print_templates() {

		// allow devs to use their own templates for container and/or comment by including their template path.
		$container_path = apply_filters( 'ajaxify_comments_container_template', "{$this->template_path}/container.tmpl.php" );
		$comment_path   = apply_filters( 'ajaxify_comments_comment_template', "{$this->template_path}/comment.tmpl.php" );

		if ( ! file_exists( $container_path ) || ! file_exists( $comment_path ) ) {
			return false;
		}

		// get templates.
		?>
		<script type="text/template" id="ajaxify-comments-container-tmpl"><?php require_once $container_path; ?></script>
		<script type="text/template" id="ajaxify-comments-comment-tmpl"><?php require_once $comment_path; ?></script>
		<?php
	}

}