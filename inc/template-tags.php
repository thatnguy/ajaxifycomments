<?php
	function the_ajax_comments( $args = array() ) {
		
		// check if password is required or password is matched.
		if ( post_password_required() ) {
			return;
		}

		$instance = Ajaxify_Comments::get_instance();
		
		$instance->set_post( get_the_ID() );

		foreach ( array( 'per_page', 'orderby', 'order' ) as $checking_arg ) {
			if ( isset( $args[ $checking_arg ] ) ) {
				$instance->set_arg( $checking_arg, $args[ $checking_arg ] );
			}
		}

		$instance->render();
	}