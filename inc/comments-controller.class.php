<?php
/**
 * An addition to the WP REST API plugin's default comments controller
 * The addition came upon the idea of paginating comments based on comments
 * with no parents - we shall call these main comments.
 * 		e.g. per_page will refer to number of main comments NOT the inclusive of sub-comments
 * 		
 */
class Ajaxify_Comments_Controller extends WP_REST_Comments_Controller {

	public function __construct( $namespace = '' ) {
		parent::__construct();

		$this->namespace = $namespace;
	}

	public function register_routes() {

		register_rest_route( $this->namespace, '/' . $this->rest_base, array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
				'args' => $this->get_collection_params()
			),
			array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => array( $this, 'create_item' ),
				'permission_callback' => array( $this, 'create_item_permissions_check' ),
				'args'     => $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
			),
			'schema' => array( $this, 'get_public_item_schema' )
		) );
	}

	/**
	 * Get list of main comments and their sub comments
	 * 
	 * @param  WP_REST_Request $request Full details of the request
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$prepared_args = array(
			'author_email'    => isset( $request['author_email'] ) ? $request['author_email'] : '',
			'comment__in'     => $request['include'],
			'comment__not_in' => $request['exclude'],
			'karma'           => isset( $request['karma'] ) ? $request['karma'] : '',
			'number'          => $request['per_page'],
			'post__in'        => $request['post'],
			'parent__in'      => $request['parent'],
			'parent__not_in'  => $request['parent_exclude'],
			'search'          => $request['search'],
			'offset'          => $request['offset'],
			'orderby'         => $this->normalize_query_param( $request['orderby'] ),
			'order'           => $request['order'],
			'status'          => $request['status'],
			'type'            => $request['type'],
			'no_found_rows'   => false,
			'author__in'      => $request['author'],
			'author__not_in'  => $request['author_exclude'],

			// will fetch main comments (parent = 0) if no parent specified.
			'parent'          => isset( $request['parent'][0] ) ? $request['parent'][0]: 0
		);

		/**
		 * devs can customise the args for retrieving comments.
		 * example: we can order our comments by meta values using 'orderby' => 'meta_value', 'meta_key' => '<your_meta_key>'
		 */
		$prepared_args = apply_filters( 'ajaxify_comments_rest_prepared_args', $prepared_args );

		// work out the number of items to get based on per_page and current page number
		if ( empty( $request['offset'] ) ) {
			$prepared_args['offset'] = $prepared_args['number'] * ( absint( $request['page'] ) - 1 );
		}

		/**
		 * Note that WP query will help us with filtering out unapproved comments
		 * and then adjusting the offset and limit on those result
		 * making our life easier :)
		 */
		$query = new WP_Comment_Query;
		$result = $query->query( $prepared_args );

		$comments = array();
		foreach ( $result as $comment ) {
			if ( ! $this->check_read_permission( $comment ) ) {
				continue;
			}

			// @todo add filter to the $comment data.

			// We want to parse response to the main comment's children too
			$comments[] = $this->prepare_response_recursive( $comment, $request );
		}

		unset( $prepared_args['number'] );
		unset( $prepared_args['offset'] );

		// get the total number of main comments
		$prepared_args['count'] = true;
		$total_comments = $query->query( $prepared_args );

		$max_pages = ceil( $total_comments / $request['per_page'] );

		$response = rest_ensure_response( $comments );
		$response->header( 'X-WP-Total', $total_comments );
		$response->header( 'X-WP-TotalPages', $max_pages );

		return $response;
	}

	function create_item( $request ) {
		/**
		 * This further checks if parent exists when replying to a comment.
		 */
		$parent = $request['parent'];
		if ( $parent && 0 != $parent ) {

			$parent = get_comment( $parent );
			if ( ! $parent || 'trash' === $parent->comment_approved ) {
				return new WP_Error( 'rest_comment_failed_create', __( 'Parent comment does not exist.' ), array( 'status' => 500 ) );
			}
		}

		return parent::create_item( $request );
	}

	/**
	 * Recursively prepare response
	 * @param  WP_Comment $comment
	 * @param  WP_REST_Request $request
	 * @return prepared response comment.
	 */
	function prepare_response_recursive( $comment, $request ) {

		// call $comment->get_children() first as prepare_item_for_response() will alter the $comment->children to count
		// but we want WP_Comment objects.
		$children = $comment->get_children();
		$data     = $this->prepare_item_for_response( $comment, $request );
		$_comment = $this->prepare_response_for_collection( $data );
		$_comment['child_count'] = count( $children );

		// set 'children' despite having no children.
		// this is so we can let BackboneJS handle the attribute changes for children.
		$_comment['children'] = array();

		if ( isset( $request['children_fetch_count'] ) ) {
			$children = array_slice( $children, 0, (int) $request['children_fetch_count'] );
			
			foreach ( $children as $child ) {
				if ( ! $this->check_read_permission( $child ) ) {
					continue;
				}

				$_comment['children'][] = $this->prepare_response_recursive( $child, $request );
			}
		}

		return $_comment;
	}

}