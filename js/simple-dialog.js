/**
 * Just a simple dialog box.
 * This class was created because javascript's native dialogs
 * are ugly :)
 * 
 * dialog boxes: alert, confirm, prompt
 * 
 * @requires  jQuery, UnderscoreJS
 * 
 */
;( function( $ ) {

	// constructor
	var SimpleDialog = function( options ) {

		var dialogButtons = {
			alert: {},
			confirm: {},
			prompt: {}
		};

		var self = this;

		var dialogWindow = null;
		var footer       = null;
		var overlayElem  = null;

		// default settings
		var settings = {
			type: 'alert',
			title: '',
			content: '',
			class: 'simple-dialog',
			overlay: true
		};

		var initialize = function() {
			options = typeof options === 'undefined' ? {} : options;

			_.extend( settings, options );

			// reset chosen type if user chosen type is not a valid type
			if ( ! dialogButtons.hasOwnProperty( settings.type ) ) {
				options.type = 'alert';
			}

			// we set our own title if there is an empty title
			if ( settings.title === '' ) {
				settings.title = options.type.charAt(0).toUpperCase() + options.type.substring(1) + ' Dialog';
			}

			var pageSize = getPageSize();
			pageSize[0] = pageSize[0] / 2;

			// height should be adjusted based on the content inside the container
			dialogWindow = $( '<div class="simple-dialog-container ' + settings.class + ' ' + settings.type + '" style="width:' + pageSize[0] + 'px;" >' );
			dialogWindow.css({
				marginLeft: '-' + parseInt( ( pageSize[0] / 2 ) , 10 ) + 'px',
				marginTop: '-' + parseInt( 100, 10 ) + 'px'
			});

			footer = $( '<div class="simple-dialog-footer" />' );

			createDialogBody();

			// setup default button properties
			var btnProps = dialogButtons[settings.type];

			if ( settings.type === 'alert' ) {
				btnProps.close = {
					name: 'Close',
					callback: close
				};
			} else if ( settings.type === 'confirm' || settings.type === 'prompt' ) {
				btnProps.cancel = {
					name: 'Cancel',
					callback: close
				};
				btnProps.ok = {
					name: 'Ok',
					callback: close // just a dummy
				};
			}

			// check if user wants to override the button properties
			if ( settings.hasOwnProperty( 'buttons' ) ) {
				setButtonProperties( settings.buttons );
			} else {
				setupFooterButtons();
			}

			if ( settings.overlay ) {
				createOverlayElement();
			}
		};

		// assembling the dialog
		var createDialogBody = function() {
			var header = $( '<div class="simple-dialog-header"><div class="title">' + settings.title + '</div>' );

			var closeBtn = $( '<div class="close" />' );
			closeBtn.on( 'click', close );
			header.append( closeBtn );

			var body = '<div class="simple-dialog-body">';
			// body content layout
			if ( settings.type === 'alert' ) {
				body += createAlertBody();
			} else if ( settings.type === 'confirm' ) {
				body += createConfirmBody();
			} else if ( settings.type === 'prompt' ) {
				body += createPromptBody();
			}

			body += '</div>';

			dialogWindow.append( header ).append( body );
		};

		var createAlertBody = function() {
			return '<div class="simple-dialog-content">' + options.content + '</div>';
		};

		var createConfirmBody = function() {
			return '<div class="simple-dialog-content">' + options.content + '</div>';
		};

		var createPromptBody = function() {

		};
	
		var createOverlayElement = function() {
			overlayElem = $( '<div class="simple-dialog-overlay" />' );
			overlayElem.on( 'click', close );
		};

		var setupFooterButtons = function() {
			footer.empty();

			_.each( dialogButtons[settings.type], function( value, key, list ) {
				var b = $( '<button class="btn-' + key + '">' + list[key].name + '</button>' );
				b.on( 'click', { callback: list[key].callback }, _callback );

				footer.append( b );
			});
		};

		var show = function() {
			var body = $( 'body' );

			if ( overlayElem !== null ) {
				body.append( overlayElem );
			}

			body.append( dialogWindow.append( footer ) );
		};

		// close the dialog
		var close = function() {
			dialogWindow.detach();

			if ( overlayElem !== null ) {
				overlayElem.detach();
			}
		};

		var _callback = function( e ) {
			var cb = e.data.callback;

			/**
			 * Here we can pass in arguments we want the given callback to have.
			 */
			cb( this );
		};

		var setButtonProperties = function( prop ) {
			if ( ! _.isObject( prop ) ) {
				return false;
			}

			var d = dialogButtons[options.type];
			_.each( prop, function( value, key, list ) {
				if ( d.hasOwnProperty( key ) ) {
					if ( ! _.isObject( value ) || _.isEmpty( value ) ) {
						return;
					}

					// check name
					if ( value.hasOwnProperty( 'name' ) ) {
						var v = value.name;

						if ( _.isString( v ) && v !== d[key].name ) {
							d[key].name = v;
						}
					}

					// custom callback
					if ( value.hasOwnProperty( 'callback' ) ) {
						var c = value.callback;
						if ( ! _.isFunction( c ) ) {
							return;
						}

						d[key].callback = c;
					}
				}
			});

			// when setup button properties we need to assemble the dialog buttons for the footer
			setupFooterButtons();
		}

		// from Thickbox
		var getPageSize = function() {
			var de = document.documentElement;

			// window viewport
			var w = window.innerWidth || self.innerWidth || ( de && de.clientWidth ) || document.body.clientWidth;
			var h = window.innerHeight || self.innerHeight || ( de && de.clientHeight ) || document.body.clientHeight;

			arrayPageSize = [w,h];

			return arrayPageSize;
		};

		var getDialog = function() {
			return this;
		};

		initialize();

		return {
			show: show,
			close: close,
			getDialog: getDialog,
			setButtonProperties: setButtonProperties
		};

	};

	// make this class available globally
	window.SimpleDialog = SimpleDialog;

} )( jQuery );