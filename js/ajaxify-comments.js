;( function( root, doc, undefined ) {

	var getFirstChildElement = function( node ) {
		var childNodes = node.childNodes, i = 0;

		while ( node = childNodes[i++] ) {
			if ( 1 === node.nodeType ) {
				return node;
			}
		}

		return null;
	}

	var getNodeIndex = function( node ) {
		var i = 0;

		while ( null != ( node = node.previousSibling ) ) i++;

		return i;
	};

	var emptyNode = function( node ) {
		while ( node.firstChild ) {
			node.removeChild( node.firstChild );
		}

		return node;
	}
	
	var clone = function( obj ) {

		if ( ! _.isObject( obj ) || _.isFunction( obj ) ) {
			return obj;
		}

		if ( _.isArray( obj ) ) {
			return obj.slice();
		}

		var cloneObj = {};

		for ( var key in obj ) {
			cloneObj[ key ] = clone( obj[ key ] );
		}

		return cloneObj
	};

	// holds localized data.
	var _data = {
		request: {
			post: ajaxify_comments_data.post,
			perPage: parseInt( ajaxify_comments_data.per_page ),
			orderBy: ajaxify_comments_data.orderby,
			order: ajaxify_comments_data.order,
			children_fetch_count: ajaxify_comments_data.children_fetch_count
		},
		loggedIn: !!ajaxify_comments_data.user_logged_in,
		rest_nonce: ajaxify_comments_data.rest_nonce,
		urls: {
			wp: ajaxify_comments_data.api_urls['wp'],
			ajaxifyComments: ajaxify_comments_data.api_urls['ajaxify-comments']
		},

		sortables: clone( ajaxify_comments_data.sortables )
	};


	/*************
	 * TEMPLATES *
	 *************/

	var templates = {},
		template  = null;

	template = document.getElementById( 'ajaxify-comments-container-tmpl' );
	if ( _.isElement( template ) ) {
		templates.container = template.innerHTML;
	}

	template = document.getElementById( 'ajaxify-comments-comment-tmpl' );
	if ( _.isElement( template ) ) {
		templates.comment = template.innerHTML;
	}


	/**********
	 * MODELS *
	 **********/

	var Comment = Backbone.Model.extend( {

		// base url
		url: _data.urls.wp + '/comments',

		initialize: function() {

			// property for toggling (show/hide) children
			this.set( 'hide', false );

			/**
			 * Recursively create collection of comment models
			 * for child comments. 
			 */
			var children = this.get( 'children' );
			if ( children && 0 < children.length ) {
				var that = this;

				_.each( children, function( child ) {
					that.addChildren( new Comment( child ) );
				} );
			}

			this.listenTo( this, 'change:children', this.updateChildren );

		},

		updateChildren: function() {
			var changed = this.changedAttributes(),
				that    = this;

			if ( ! this.children && 0 < changed.children.length ) {
				_.each( changed.children, function( comment ) {
					that.addChildren( comment );
				} );

			} else if ( this.children ) {
				this.children.reset( changed.children );
			}
		},

		/**
		 * Add a child comment to a comment.
		 * 
		 * @param Comment child comment
		 */
		addChildren: function( comment ) {
			if ( ! this.hasOwnProperty( 'children' ) ) {
				this.children = new Comments;
				this.children.add( comment );

				// keep reference of parent model.
				// @note circular ref
				this.children.parentModel = this;

				// announce that a comment becomes a parent comment.
				this.trigger( 'newParent' );
			} else {
				this.children.add( comment ); 
			}
		},

		/**
		 * Override the default sync
		 * we want to use a different url for creating comment
		 * also adding our own request headers.
		 */
		sync: function( method, model, options ) {

			if ( 'update' === method || 'delete' === method ){
				options.url = this.url + '/' + model.get( 'id' )
			} else if ( 'create' === method ) {
				options.url = _data.urls.ajaxifyComments + '/comments';
			}

			// WP-API HTTP cookie authentication.
			options.beforeSend = function( xhr ) {
				xhr.setRequestHeader( 'X-WP-Nonce', _data.rest_nonce );
			};

			options.error = function( model, response, options ) {/* notify errors */};

			Backbone.sync( method, model, options );
		}

	} );


	/***************
	 * COLLECTIONS *
	 ***************/
	
	var Comments = Backbone.Collection.extend( {

		model: Comment,

		url: _data.urls.ajaxifyComments + '/comments',

		initialize: function() {
			// each collection has their own request params.
			this.params = {
				per_page: _data.request.perPage,
				orderby: _data.request.orderBy,
				order: _data.request.order
			};

			// fixed per fetch amount.
			this.perFetch = _data.request.perPage;

			// total children count.
			this.total = 0;

		},

		/**
		 * when fetching comments from server
		 * we want to pass in initial datas that are commonly used.
		 */
		fetch: function( options ) {
			options = 'undefined' === typeof options ? {} : options;

			// we should ignore empty params.
			var params = {};
			_.each( this.params, function( value, key ) {
				if ( _.isString( value ) && _.isEmpty( value ) ) {
					return;
				}

				params[ key ] = value;
			} );

			options.data = 'undefined' !== typeof options.data ? _.extend( options.data, params ) : params;

			options.data.post = _data.request.post;
			options.data.children_fetch_count = _data.request.children_fetch_count;

			// fetch comments with parent 0 as default.
			// if collection has parent model we use that model id as parent.
			options.data.parent = 0;
			this.parentModel && ( options.data.parent = this.parentModel.get( 'id' ) );

			// WP API nonce
			options.beforeSend = function( xhr ) {
				xhr.setRequestHeader( 'X-WP-Nonce', _data.rest_nonce );
			};

			options.success = function( collection, response, options ) {
				var xhr = options.xhr;

				// update collection total count
				collection.total = parseInt( xhr.getResponseHeader( 'X-WP-Total' ) );

				options.callback && options.callback( collection );

				// reset the ignore prepend prop
				if ( collection.params.ignorePrepend ) {
					collection.params.ignorePrepend = false;
				}

				Backbone.trigger( 'ajaxify-comments:comments-fetched', collection );
			};

			/**
			 * call Backbone's fetch in this object's context
			 * we use 'return' because backbone returns a jQuery promise
			 * which we might need later.
			 */
			return Backbone.Collection.prototype.fetch.call( this, options );
		},

		/**
		 * Recursively get a comment even
		 * going down to descendants.
		 */
		getModel: function( id ) {
			var model = Backbone.Collection.prototype.get.call( this, id ),
				len   = this.length;

			if ( model ) {
				return model;
			}

			if ( 0 < len ) {
				for ( var i = 0; i < len; i++ ) {
					var m = this.models[i];

					if ( _.has( m, 'children' ) && 0 < m.children.length ) {
						return m.children.getModel( id );
					}
				}
			}

			return false;
		},

		/**
		 * create or save a comment based on the current number of comments allowed for the list.
		 *
		 * @param attrs Comment attributes for creating|saving a comment.
		 * @param callback additional success callback to be called upon success.
		 */
		createOrSave: function( attrs, callback ) {
			var that = this;

			var successCallback = function( model, attrs ) {
				// increase the total counter to reflect the server.
				that.total++;

				Backbone.trigger( 'ajaxify-comments:comment-saved', model.attributes );

				_.isFunction( callback ) && callback();

				// tell listeners about the updated children count.
				that.trigger( 'change:total', that );
			};

			// always wait for server to respond with return model data before
			// adding to collection.
			if ( this.length < this.params.per_page ) {
				this.create( attrs, { wait: true, success: successCallback } );
			} else {
				// save to server without adding to collection.
				var comment = new Comment( attrs );

				comment.save( {}, { wait: true, success: successCallback } );
			}
		},

		/**
		 * fetch comments
		 */
		load: function( options ) {
			options = options || {};

			if ( ! options.ignorePerFetch ) {
				this.params.per_page += this.perFetch;
			}

			if ( options.ignorePrepend ) {
				this.params.ignorePrepend = true;
			}

			this.fetch( options );
		}

	} );


	/*********
	 * VIEWS *
	 *********/

	var CommentView = Backbone.View.extend( {

		tagName: 'div',

		className: 'comment-item',

		template: _.template( templates.comment ),

		events: {
			'click .comment-reply'   : 'controlReply',
			'click .comment-edit'    : 'controlEdit',
			'click .comment-delete'  : 'controlDelete',
			'click .toggle-children' : 'toggleEv'
		},

		initialize: function() {
			this.el.className += ' comment-item-' + this.model.id;

			this.listenTo( this.model, 'newParent', this.renderChildren );
			this.listenTo( this.model, 'change:content', this.renderContent );
			this.listenTo( this.model, 'change:hide', this.toggleChildren );
			this.listenTo( this.model, 'change:children', this.renderChildren );
			this.listenTo( this.model, 'change:human_date', this.renderContent );
			this.listenTo( this.model, 'remove', this.removeComment );
			this.listenTo( Backbone, 'ajaxify-comments:comment-' + this.model.id, this.doEvent );
		},

		render: function() {
			var children = this.model.children;

			emptyNode( this.el );

			this.content = document.createElement( 'div' );
			this.content.className = 'comment-item-content';
			this.el.appendChild( this.content );
			this.renderContent();

			this.renderControls();
			this.renderChildren();

			return this;
		},

		renderChildren: function() {

			if ( ! this.model.children ) {
				return this;
			}

			if ( ! this.model.children.total ) {
				this.model.children.total = this.model.get( 'child_count' );
			}

			if ( ! this.commentChildren ) {
				this.commentChildren = document.createElement( 'div' );
				this.commentChildren.className = 'comment-children';
				this.el.appendChild( this.commentChildren );
			}

			// empty out the child comments section if we have some.
			if ( 0 < this.commentChildren.childNodes.length ) {
				emptyNode( this.commentChildren );
			}

			// render the toggle (hide/show) children.
			if ( 0 < this.model.children.length ) {
				var childrenToggle = document.createElement( 'div' );
				childrenToggle.className = 'toggle-children';
				childrenToggle.innerText = 'hide replies';

				this.commentChildren.appendChild( childrenToggle );
			}

			var commentsView = new CommentsView( { collection: this.model.children } );
			this.commentChildren.appendChild( commentsView.render().el );

			return this;
		},

		renderContent: function( model, value ) {
			this.content.innerHTML = this.template( this.model.toJSON() );
		},

		renderControls: function() {

			this.controls = {
				wrapper: null,
				reply: null,
				edit: null,
				delete: null
			};

			// control wrapper
			this.controls.wrapper = document.createElement( 'div' );
			this.controls.wrapper.className = 'comment-controls';

			if ( _data.loggedIn ) {

				// reply control.
				this.controls.reply = document.createElement( 'button' );
				this.controls.reply.className = 'comment-reply';
				this.controls.reply.setAttribute( 'type', 'button' );

				this.controls.wrapper.appendChild( this.controls.reply );
			}

			if ( this.canMod() ) {

				// edit control
				this.controls.edit = document.createElement( 'button' );
				this.controls.edit.className = 'comment-edit';
				this.controls.edit.setAttribute( 'type', 'button' );

				// delete control
				this.controls.delete = document.createElement( 'button' );
				this.controls.delete.className = 'comment-delete';
				this.controls.delete.setAttribute( 'type', 'button' );

				this.controls.wrapper.appendChild( this.controls.edit );
				this.controls.wrapper.appendChild( this.controls.delete );
				this.el.insertBefore( this.controls.wrapper, this.el.childNodes[0] );
			}
		},

		toggleChildren: function( model, value ) {
			var action = value ? 'add' : 'remove';
			this.commentChildren.classList[ action ]( 'hide' );
		},

		removeComment: function() {
			this.remove();
		},

		// hide/show children comments
		toggle: function( hide ) {

			hide = hide || ( this.model.get( 'hide' ) ? false : true );

			this.model.set( 'hide', hide );
		},

		canMod: function() {
			return this.model.get( 'can_moderate' );
		},

		toggleEv: function( e ) {

			this.toggle();

			return false;
		},

		// backbone events.

		doEvent: function() {
			var args = arguments,
				eventName = args[0] + 'Event',
				callback = this[eventName];

			if ( callback ) {
				callback.apply( this, Array.prototype.splice.call( args, 1 ) );
			}
		},

		saveEvent: function( attrs ) {
			if ( ! this.model.children ) {
				this.model.children = new Comments;
			}

			var that = this;
			this.model.children.createOrSave( attrs, function() {
				that.renderChildren();
			} );
		},

		updateEvent: function( attrs ) {

			// check if user can mod this comment.
			if ( ! this.canMod() ) return false;

			attrs.parent = this.model.get( 'parent' ) || 0;

			var changes = {},
				comment = this.model;

			_.each( attrs, function( value, key ) {
				var _value = comment.get( key );
				
				if ( _value !== value ) {
					changes[key] = value;
				}
			} );

			comment.save( changes, { wait: true, success: function( comment ) {
				Backbone.trigger( 'ajaxify-comments:comment-updated', comment.attributes );
			} } );
		},

		deleteEvent: function() {

			// check if user can mod this comment
			if ( ! this.canMod() ) return false;

			var collection = this.model.collection;

			this.model.destroy( {
				success: function( model, response, options ) {

					if ( collection ) {
						// update the children counter to reflect the server.
						collection.total--;
						var options = {};

						options.ignorePrepend = true;

						collection.load( options );
					}

					Backbone.trigger( 'ajaxify-comments:comment-removed', model.attributes );
				}
			} );
		},

		toggleEvent: function( hide ) {
			this.toggle( hide );
		},

		controlReply: function() {
			Backbone.trigger( 'ajaxify-comments:comment-reply', this.el, this.model.get( 'id' ) );

			return false;
		},

		controlEdit: function() {
			Backbone.trigger( 'ajaxify-comments:comment-edit', this.el, this.model.get( 'id' ) );

			return false;
		},

		controlDelete: function() {
			Backbone.trigger( 'ajaxify-comments:comment-trash', this.el, this.model.get( 'id' ) );

			return false;
		}
	} );
	
	var CommentsView = Backbone.View.extend( {

		tagName: 'div',

		className: 'comments-list',

		events: {
			'click .comments-loader' : 'loadComments'
		},

		initialize: function() {

			this.listenTo( this.collection, 'add', this.addComment );
			this.listenTo( this.collection, 'sync', this.updateLoader );
			this.listenTo( this.collection, 'change:total', this.updateLoader );
			this.listenTo( this.collection, 'reset', this.render );

			// wrapper for comment items.
			this.items = document.createElement( 'div' );
			this.items.className = 'comments-items';
			this.el.appendChild( this.items );

			// wrapper for comments loader.
			this.loader = document.createElement( 'div' );
			this.loader.className = 'comments-loader hide';
			this.loader.innerText = 'Load Comments';
			this.el.appendChild( this.loader );

			this.disableLoader = false;
			this.counter = 0;

		},

		// render collection of comments.
		render: function() {
			var that = this;

			// first clear comments
			emptyNode( this.items );

			this.collection.each( function( comment ) {
				that.addComment( comment );
			} );

			return that;
		},

		addComment: function( comment, collection, options ) {

			// keep track of the number of comments added.
			// this is used in situation where a new comment is fetched from server
			// which is not contain on the front end.
			if ( options ) {
				this.counter++;
			}

			var append = true;

			if ( ! this.collection.params.ignorePrepend ) {
				append = this.toAppend();

				if ( ! append && 0 < this.collection.length ) {
					/**
					 * There is a situation where a new comment is fetched from server
					 * and added but the comment is not `new` so would be wise to append
					 * instead of prepending.
					 *
					 * e.g. 
					 * the server is polling, we are prepending comments ( date desc ) and then we delete the newest comment
					 * after server fetches comments, Backbone will smartly merge comments
					 * therefore removing the deleted comment (newest comment) and add any new comment from server to collection.
					 * However this newly added comment will be at the last index and should be append instead of prepending.
					 */
					var c = this.collection.at( this.collection.length - this.counter );
					if ( c && comment == c ) append = true;
				}
			}

			var commentView = new CommentView( { model: comment } );

			if ( append ) {
				this.items.appendChild( commentView.render().el );
			} else {
				this.items.insertBefore( commentView.render().el, this.items.childNodes[ this.counter - 1 ] );
			}

			return this;
		},

		// called after fetching from server and synced.
		updateLoader: function( collection ) {

			// update loader
			var canLoad = collection.total > collection.length;
			canLoad ? this.loader.classList.remove( 'hide' ) : this.loader.classList.add( 'hide' );
			this.disableLoader = !canLoad;

			this.counter = 0;

		},

		loadComments: function( e ) {

			var options = {};

			if ( this.disableLoader ) {
				return false;
			}

			if ( ! _.has( options, 'ignorePrepend' ) ) {
				options.ignorePrepend = true;
			}

			this.collection.load( options );

			return false;
		},

		toAppend: function() {
			var orderBy = this.collection.params.orderby,
				order   = this.collection.params.order;

			return 'asc' === order ? _data.sortables[ orderBy ] : !_data.sortables[ orderBy ];

		}

	} );

	var ContainerView = Backbone.View.extend( {

		template: _.template( templates.container ),

		initialize: function() {
			
			this.comments = new Comments;

			var tmpl = this.template();

			if ( ! _.isEmpty( tmpl ) ) {

				// test if a valid element.
				var div = document.createElement( 'div' );
				div.innerHTML = tmpl;

				var fChild = getFirstChildElement( div );

				if ( fChild ) {
					this.el = fChild;
				}

			}

			var commentsView = new CommentsView( { collection: this.comments } );

			this.el.appendChild( commentsView.el );

			this.listenTo( this.comments, 'sync', this.init );

			// is initially synced to the server?
			this.isLoaded = false;

		},

		init: function() {
			if ( ! this.isLoaded ) {
				this.isLoaded = true;

				Backbone.trigger( 'ajaxify-comments:init' );
			}
		},

		loadComments: function( soft ) {
			soft = 'undefined' !== typeof soft ? soft : true;

			var options = {};

			if ( ! soft ) {
				options.reset = true;
			}

			options.ignorePrepend = !soft;

			options.callback = function() {
				Backbone.trigger( 'ajaxify-comments:comments-loaded' );
			};

			options.ignorePerFetch = true;

			this.comments.load( options );
		}

	} );

	// start here.

	var startPoint 	   = document.getElementById( 'ajaxify-comments-startpoint' ),
		containerView  = new ContainerView;

	// no starting point found..
	if ( ! startPoint ) {
		return;
	}

	// start off by fetching root comments.
	containerView.loadComments( false );		

	// replacing startPoint with container.
	var parent = startPoint.parentNode;
	parent.replaceChild( containerView.el, startPoint );

	// cache empty string RegEx.
	var contentRegEx = /^(<p>(\s)*<\/p>)*$/;

	function API() {

		var o = {},
			t = false;

		var validateMessage = function( message ) {
			if ( message.match( contentRegEx ) ) {
				console.log( 'avoid the emptiness...' );
				return false;
			}

			// @todo validation ...

			return true;
		};

		var walk = function( comments, callback ) {

			if ( ! _.isFunction( callback ) || ! comments ) {
				return false;
			}

			comments.each( function( comment ) {
				callback( comment );

				if ( comment.children && 0 < comment.children.length ) {
					walk( comment.children, callback );
				}
			} );
		};

		var polling = function( time ) {
			containerView.loadComments( true );
			console.log('do_polling')
			var timeframe = time || 10000;

			t = setTimeout( function() {
				polling( timeframe );
			}, timeframe );
		}

		o.saveComment = function( message, id ) {

			if ( ! _data.loggedIn ||  ! validateMessage( message ) ) {
				return false;
			}

			id = id || 0;
			
			// default comment attributes.
			var attrs = {
				content: message,
				parent: id,
				type: 'comment',
				post: _data.request.post
			};

			if ( 0 === id ) {
				containerView.comments.createOrSave( attrs );
			} else {
				Backbone.trigger( 'ajaxify-comments:comment-' + id, 'save', attrs );
			}
		};

		o.updateComment = function( id, attrs ) {

			attrs = attrs || {};

			var content = attrs.content ? attrs.content : '';

			if ( ! _data.loggedIn || ! content || ( content && ! validateMessage( content ) ) ) {
				return false;
			}

			var attrs = {
				content: content,
				type: '',
				post: _data.request.post,
				type: 'comment'
			};

			Backbone.trigger( 'ajaxify-comments:comment-' + id, 'update', attrs );
		};

		o.removeComment = function( id ) {

			if ( ! _data.loggedIn ) return false;

			Backbone.trigger( 'ajaxify-comments:comment-' + id, 'delete' );
		};

		/**
		 * Sort main comments.
		 * Make sure that orderBy is within `sortables`
		 * @param  string orderBy the @check `sortables`
		 * @param  string order asc|desc
		 */
		o.sortComments = function( orderBy, order ) {

			// check if `orderBy` is within sortables
			if ( ! _data.sortables[ orderBy ] || -1 === [ 'asc', 'desc' ].indexOf( order ) ) {
				return false;
			}

			containerView.comments.params.order   = order;
			containerView.comments.params.orderby = orderBy;

			this.reload();
		};

		/**
		 * Do a hard reload by resetting and fetching main comments.
		 */
		o.reload = function() {
			containerView.loadComments( false );
		};

		/**
		 * get list of sortables.
		 */
		o.getSortables = function() {
			return _.keys( _data.sortables );
		};

		/**
		 * get total number of comments. If `id` is specified then will get the
		 * total number of comments for that comment ( comment + nested children )
		 * otherwise get all comments.
		 *
		 * @note this will only get total number of comments available on the front end (that have been fetched)
		 */
		o.getNumberOfComments = function( id ) {
			var total = 0, comments = containerView.comments;

			if ( 'undefined' !== typeof id ) {
				id = parseInt( id );
				var comment = containerView.comments.getModel( id );
				comments = comment ? comment.children : false;
				total++;
			}

			if ( comments ) {
				total += comments.total;
				walk( comments, function( comment ) {
					if ( comment.children ) {
						total += comment.children.total;
					}
				} );
			}

			return total;
		};

		o.toggleComments = function( id, hide ) {
			hide = 'undefined' !== typeof hide ? hide : false;

			Backbone.trigger( 'ajaxify-comments:comment-' + id, 'toggle', hide );
		};

		o.startPolling = function( time ) {
			// already polling ?
			if ( t ) return false;

			t = setTimeout( function() {
				polling( time );
			}, time || 10000 );
		};

		o.stopPolling = function() {
			t && clearTimeout( t );
		};

		o.listenTo = function( eventName, callback ) {
			Backbone.listenTo.call( o, Backbone, eventName, callback );
		};
		
		return o;
	}

	root.AjaxifyComments = API();

} )( window, document );