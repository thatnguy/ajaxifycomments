# README #

A WordPress plugin to turn the old tradition comment system to AJAX friendly.

### Dependencies ###
* WP-Rest plugin for making our comment system API RESTful. Check it out [here](https://wordpress.org/plugins/rest-api/)
* BackboneJS (already comes with WordPress Core) as our front end framework for handling comments in a REST API environment.

### How do I get set up? ###
* Make sure you have the dependencies listed above installed first.
* Add this plugin into your WordPress plugin folder (just create a folder in your WP plugin folder and copy these files to that new folder, then activate this plugin.)

### How to use ###
In of your theme's template files just call *the_ajax_comments()* in any section of your
template content after *get_header()* has been called.

### Filter Hooks ###
* *ajaxify_comments_sortables* - list of comment fields to order the comments on display.
                                 Here are the list of fields you can sort by:
                                 *date*, *date_gmt*, *id*, *include*, *post*, *parent*, *type*
* *ajaxify_comments_asc_prepend* - whether the sortables (listed above) ASC order will append or prepend the comments (append by default)
* *ajaxify_comments_enqueue_scripts* - Use this filter to enqueue your own front end scripts (check out below for an example how to enqueue your own scripts)

### Enqueue your own scripts ###
```
#!php
add_filter( 'ajaxify_comments_enqueue_scripts', function( $scripts ) {
	$my_script = array( 
		'url' => get_template_directory_uri() . '/js/ajaxify-comments-script.js',
		'dep' => array(),
		'type' => 'js',
	);

	$scripts['my-script'] = $my_script;

	return $scripts;
} );
```
each script registered needs to have a unique handler.
*url* contains the url location of your script file.
*dep* contains any other scripts (using handler) that this script depends on
*type (js|css)* whether this script is JS or CSS. CSS script will always be registered in the head of the document.

### Client Side Handling ###
You can handle comments using front end API.
However make sure you handle these calls inside *ajaxify-comments:init* event.
This will guarantee that your call will be done after the Comments have been loaded.

e.g.
```
#!javascript
AjaxifyComments.listenTo( 'ajaxify-comments:init', function() {
    // do stuff..
} );

```

### Client Side API functions ###

* *saveComment(message[,comment_id])* - Create a new comment or reply to a comment if the parent comment id is provided (comment_id)
* *updateComment(comment_id, <Object> attributes)* - update an existing comment with new attributes.
Attributes properties include - content (message), post (ID of post this comment belongs to).
* *removeComment(comment_id)* - remove an existing comment.
* *sortComments(orderBy, order)* - orderBy must be one of the following (date, date_gmt, id, parent, type, include) and orderBy must be either asc or desc.
* *reload()* - hard reload of all comments.
* *getSortables()* - return list of sortables that the comments can be sorted by.
* *toggleComments(comment_id[,<boolean>hide])* - show or hide replies of a comment parent.
* *startPolling(<Number>interval) - start polling (fetching comments regularly) for comments in <interval> period. interval is defined in milliseconds.
* *stopPolling()* - end polling.
* *listenTo(eventName, <Function>callback) - listen for an event.

### Events ###

```
#!javascript
// These events are called.

// after a comment is saved.
AjaxifyComments.listenTo( 'ajaxify-comments:comment-saved', function( comment ) {
});

// after a comment has been removed.
// the comment's attributes are passed to the callback.
AjaxifyComments.listenTo( 'ajaxify-comments:comment-removed', function( comment ) {
});

// after a comment is updated.
AjaxifyComments.listenTo( 'ajaxify-comments:comment-updated', function( comment ) {
});

// controls events.
// these controls events are handled by you and gives you the freedom to define
// what they do. By default they do nothing.

// when user clicks to reply to a comment
AjaxifyComments.listenTo( 'ajaxify-comments:comment-reply', function(el, id) {
});

// when user clicks to edit a comment
AjaxifyComments.listenTo( 'ajaxify-comments:comment-edit', function(el, id) {
});

// when user clicks to remove a comment.
AjaxifyComments.listenTo( 'ajaxify-comments:comment-trash', function(el, id) {
});

// Called after Ajaxify Comments is initiated and main comments have been fetched.
// Every function called should reside in this event callback.
// note `this` context is AjaxifyComments.
AjaxifyComments.listenTo( 'ajaxify-comments:init', function() {
    this.saveComment( 'Hello World' );

    this.updateComment( 31, {content: 'ya'});
} );
```