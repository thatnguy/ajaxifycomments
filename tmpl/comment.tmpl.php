<div class="comment-wrapper__author">
	<div class="author__author-dp"><img src="<%= author_avatar_urls['48'] %>" /></div>
</div>
<div class="comment-wrapper__content">
	<div class="content__header">
		<div class="header__author-name"><%= author_name %></div>
		<div class="header__date-created"><%= human_date %></div>
	</div>
	<div class="content__message"><%= content.rendered %></div>
</div>
